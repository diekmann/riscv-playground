// Serial port driver for the QEMU virt machine:
// 'virt'; CLINT, PLIC, 16550A UART, VirtIO, device-tree, Priv ISA v1.10
// https://github.com/riscv/riscv-qemu/wiki

// UART address taken from
// https://github.com/michaeljclark/riscv-probe/blob/891ac1df30c3cc7f49c0ac175ecb6d390ec5db34/env/virt/setup.c
// or
// https://github.com/qemu/qemu/blob/7cc0cdcd6a771010ca4a4857c4e4df966bb4e6c2/hw/riscv/virt.c#L62
const NS16550A_UART0_CTRL_ADDR: *mut u8 = 0x10000000 as *mut u8;

// Calling this function should only inline a few assembly instructions,
// no assumptions about the stack or registers are required.
// Used for early boot and panic printing.
#[naked] // this functio can be called in _start, where the stack is not ready.
#[inline(always)]
pub unsafe fn raw_write(c: u8) {
    // This function MAY be called before init().
    // Just writing to the UART without initialization works on QEMU.
    // On a real device, this may not work and init() must be called.

    // The store must be volatile and not optimized away.
    // Also, the store must be inlined, since the stack may not be setup!
    core::intrinsics::volatile_store(NS16550A_UART0_CTRL_ADDR, c);

    // The following also works on QEMU, in case the above intrinsic does not.
    // *NS16550A_UART0_CTRL_ADDR = c;
}


// Initialize the UART.
pub fn init(){
    // UART driver as described in
    // https://github.com/michaeljclark/riscv-probe/blob/891ac1df30c3cc7f49c0ac175ecb6d390ec5db34/libfemto/drivers/ns16550a.c#L55
    let clock_freq: u32 = 1843200;
    let baud_rate: u32 = 115200;
    let divisor = clock_freq / (16 * baud_rate);

    let line_control_register = unsafe {NS16550A_UART0_CTRL_ADDR.offset(0x03) };
    let divisor_latch_bit = 0x80;

    let divisor_lsb = unsafe {NS16550A_UART0_CTRL_ADDR.offset(0x00) };
    let divisor_msb = unsafe {NS16550A_UART0_CTRL_ADDR.offset(0x01) };

    unsafe {
        core::intrinsics::volatile_store(line_control_register, divisor_latch_bit);
        core::intrinsics::volatile_store(divisor_lsb, divisor as u8);
        core::intrinsics::volatile_store(divisor_msb, (divisor >> 8) as u8);
        core::intrinsics::volatile_store(line_control_register, 0x03 | 0x08); // a data word is 8 bit, parity odd.

        // TODO: enable interrupts. Not working. Maybe I need to setup the PLIC first?
        // https://www.lammertbies.nl/comm/info/serial-uart.html
        // https://en.wikibooks.org/wiki/Serial_Programming/8250_UART_Programming
        core::intrinsics::volatile_store(line_control_register, 0); // DLAB = 0
        core::intrinsics::volatile_store(NS16550A_UART0_CTRL_ADDR.offset(0x01), 0x1|0x02|0x04); // turn on interrupts
    }

    print_bytes(b"\r\nUART ready.\r\n");
}

// Print a single byte the safe way.
fn putc(c: u8) {
    let line_status_register = unsafe { NS16550A_UART0_CTRL_ADDR.offset(0x05) };
    unsafe {
        while (core::intrinsics::volatile_load(line_status_register) & 0x40) == 0 {
            // wait for transmit register to be ready and lint to be idle.
        };
        raw_write(c);
    }
}

// Blocks until data is available.
pub fn getc() -> u8 {
    let line_status_register = unsafe { NS16550A_UART0_CTRL_ADDR.offset(0x05) };
    //unsafe{asm!("wfi")};
    loop {
        unsafe {
            if (core::intrinsics::volatile_load(line_status_register) & 0x01) == 0x01 {
                // data available
                return core::intrinsics::volatile_load(NS16550A_UART0_CTRL_ADDR)
            }
        }
        //idle(); //TODO: UART needs to raise interrupt.
        //unsafe{asm!("wfi" ::: "memory");}
    }
}

fn print_bytes(str: &[u8]) {
    for c in str.iter() {
        putc(*c);
    }
}

// Get the cool rust string formatting functions running.
use core::fmt::Write;

struct Uart;

impl Write for Uart {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        print_bytes(s.as_bytes());
        Ok(())
    }
}

pub fn printf(args: core::fmt::Arguments) {
    let mut uart = Uart{};
    uart.write_fmt(args).expect("writing to UART failed");
    // the .expect() is quite pointless here, since a panic! will try to print the panic and
    // recursively fail. Anyhooo, it makes the compiler warning go away. *shroug*
}

pub fn print(s: &str) {
    printf(format_args!("{}", s));
}

pub fn println(s: &str) {
    printf(format_args!("{}\r\n", s));
}

