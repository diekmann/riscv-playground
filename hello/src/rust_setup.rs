// Get everything the rust runtime needs ready. Except for memory management.
use super::uart; // only raw_write, since UART not ready?
use core::panic::PanicInfo;

#[naked]
#[inline(always)]
fn hang() -> !{
    loop{
        unsafe {
            // seems to stall qemu for ever.
            asm!("wfi"); // Wait For Interrupt.
        }
    }
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    unsafe {
        // First, a very primitive print, in case all is broken.
        uart::raw_write('P' as u8);
        uart::raw_write('A' as u8);
        uart::raw_write('N' as u8);
        uart::raw_write('I' as u8);
        uart::raw_write('C' as u8);
        uart::raw_write('\r' as u8);
        uart::raw_write('\n' as u8);
    }

    // Print a nice panic message (risking that we panic while panicking).
    uart::printf(format_args!("{}", info));

    hang();
}

#[no_mangle]
pub fn abort() -> ! {
    unsafe {
        uart::raw_write('A' as u8);
        uart::raw_write('B' as u8);
        uart::raw_write('O' as u8);
        uart::raw_write('R' as u8);
        uart::raw_write('T' as u8);
        uart::raw_write('\r' as u8);
        uart::raw_write('\n' as u8);
    }
    hang();
}

