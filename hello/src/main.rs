#![no_std]  // No implicit linking to the std lib.
#![no_main] // There is no runtime, we define our own entry point
#![feature(naked_functions)] // requires nightly
#![feature(asm)]
#![feature(core_intrinsics)] // volatile memory access

mod uart;
mod machine_setup;
mod rust_setup;

const SIFIVE_PLIC_ADDR: *const u32 = 0x0c00_0000 as *const u32;
const SIFIVE_PLIC_PRIORITY_BASE: *mut u32 = SIFIVE_PLIC_ADDR as *mut u32;

const SIFIVE_PLIC_NUM_SOURCES: isize = 127; // qemu include/hw/riscv/virt.h VIRT_PLIC_NUM_SOURCES
// source 0 to 126 (inclusive) can be accessed, but source 0 should be invalid.

use core::mem::size_of;

// why 4?
// found empirically by checking that qemu does not show `qemu-system-riscv32: plic: invalid register read`.
// Each bit in the pending/enables bit registers corresponds to one source.
const SIFIVE_PLIC_NUM_BIT_REGISTERS: isize = (SIFIVE_PLIC_NUM_SOURCES+1) / (size_of::<u32>() as isize * 8);

const SIFIVE_PLIC_PENDING_BASE: *const u32 = 0x0c00_1000 as *const u32; // SIFIVE_PLIC_ADDR + (qemu)VIRT_PLIC_PENDING_BASE
const SIFIVE_PLIC_ENABLES_BASE: *mut u32 = 0x0c00_2000 as *mut u32;

fn dump_interrupt_status_mstatus() {
    let pos_mie = 3; // M-mode Interrupts Enable position in mstatus register.
    let pos_sie = 1; // S-mode Interrupts Enable ....

    let mstatus: u32;
    unsafe {
        // Control Status Register Read
        asm!("csrr $0, mstatus" : "=r"(mstatus));
    }
    let is_set = |bit_pos: u32| -> bool {
        let mask = 0x1 << bit_pos;
        (mstatus & mask) == mask
    };
    uart::printf(format_args!("Global machine interrupts (mstatus) are {}.\r\n", on_off(is_set(pos_mie))));
    uart::printf(format_args!("Global supervisor machine interrupts (mstatus) are {}.\r\n", on_off(is_set(pos_sie))));
    uart::printf(format_args!("mstatus:{:#b}\r\n", mstatus));
}
fn dump_interrupt_status_mie() {
    let mie: isize;
    let mip: isize;
    unsafe {
        asm!("csrr $0, mie" : "=r"(mie));
        asm!("csrr $0, mip" : "=r"(mip));
    }
    uart::printf(format_args!("mie:{:#b} mip:{:#b}.\r\n", mie, mip));

    let is_set = |bit_pos: u32| -> bool {
        let mask = 0x1 << bit_pos;
        (mie & mask) == mask
    };
    uart::printf(format_args!("Machine software interrupts are {}.\r\n", on_off(is_set(3))));
    uart::printf(format_args!("Machine timer interrupts are {}.\r\n", on_off(is_set(7))));
    uart::printf(format_args!("Machine external interrupts are {}.\r\n", on_off(is_set(11))));
}
fn dump_interrupt_status_plic() {
    // The Platform-Level Interrupt Controller (PLIC) handles global interrupts and should trigger
    // external interrupts in a CPU.
    //
    // In contrast, the Core-Local INTerruptor (CLINT) handler interrupts only interesting for a
    // single core, such as timer interrupts.

    uart::println("PLIC status:");
    let mut prev_prio: u32 = 0xffffffff;
    for i in 0..SIFIVE_PLIC_NUM_SOURCES {
        // entry 0 is reserved, ..., at least according to the manual, ...
        let priority = unsafe { *SIFIVE_PLIC_PRIORITY_BASE.offset(i) };
        let meaning = match priority {
                0 => "disabled",
                1 => "enabled, lowest priority",
                2..=6 => "enabled",
                7 => "enabled, highest priority",
                _ => "invalid!!",
            };
        if prev_prio == priority && i != SIFIVE_PLIC_NUM_SOURCES-1 {
            uart::print(".");
        } else {
            uart::printf(format_args!("source {} priority: {} ({})\r\n", i, priority, meaning));
        }
        prev_prio = priority;
    }

    uart::println("??");
    uart::print("pending bits: ");
    for i in 0..SIFIVE_PLIC_NUM_BIT_REGISTERS {
        let pending_bits = unsafe { *SIFIVE_PLIC_PENDING_BASE.offset(i) };
        uart::printf(format_args!("{:#b} ", pending_bits));
    }
    uart::println("");

    uart::print("enables bits: ");
    for i in 0..SIFIVE_PLIC_NUM_BIT_REGISTERS {
        let enables = unsafe { *SIFIVE_PLIC_ENABLES_BASE.offset(i) };
        uart::printf(format_args!("{:#b} ", enables));
    }
    uart::println("");
}

fn on_off(b: bool) -> &'static str {
    if b {
        "on"
    } else {
        "off"
    }
}


fn dump_interrupt_status() {
    dump_interrupt_status_mstatus();
    dump_interrupt_status_mie();
    dump_interrupt_status_plic();
}

fn enable_interrupts(){
    // Upon reset, a hart’s privilege mode is set to M.

    dump_interrupt_status();

    // Turning on PLIC
    for i in 0..SIFIVE_PLIC_NUM_SOURCES {
        unsafe { *SIFIVE_PLIC_PRIORITY_BASE.offset(i) = 0x7 };
    }
    for i in 0..SIFIVE_PLIC_NUM_BIT_REGISTERS {
        unsafe { *SIFIVE_PLIC_ENABLES_BASE.offset(i) = 0xffffffff};
    }


    // mstatus bit 3: mie - m-mode interrupt enable
    // mstatus bit 7: mpie - m-mode previous interrupt enable
    // mstatus bits 11,12: mpp - previous privilege mode
    let mstatus_mie = 0x1 << 3;
    unsafe{
        // Control Status Register Set Bit
        asm!("csrs mstatus, $0" : : "r"(mstatus_mie));
    }
    //TODO: MEIE (bit 11) does not get set :-(
    // "For all the various interrupt types (software, timer, and external), if a privilege level is not sup-
    // ported, then the associated pending and interrupt-enable bits are hardwired to zero in the mip and
    // mie registers respectively. Hence, these are all effectively WARL fields."
    unsafe{
        // bit 7 is MTIE, required for timer interrupts.
        let bits: u32 = (0x1 << 3) | (0x1 << 7) | (0x1 << 11);
        asm!("csrs mie, $0" : : "r"(bits));
    }

    dump_interrupt_status();
}

fn dump_isa(){
    // The RISC-V Instruction Set Manual -- Volume II: Privileged Architecture

    // The misa register is Machine XLEN bits long, which can be 32, 64, or 128.
    let misa: isize;
    unsafe{
        asm!("csrr $0, misa" : "=r"(misa));
    }
    // The highest 2 bits determine the base witdth (XLEN).
    // We can check them by looking at the sign bit.
    if misa >= 0 {
        uart::println("ISA is RV32");
        if (misa >> 30) != 0x1 {
            // The misa register could also be hard-coded to 0.
            uart::println("Actually, I'm not sure what ISA this is, ....");
        }
    } else {
        uart::println("ISA is RV64 or RV128");
    }

    for (i, e) in b"ABCDEFGHIJKLMNOPQRSTUVWXYZ".iter().enumerate() {
        let mask = 0x1 << i;
        if (misa & mask) == mask {
            uart::printf(format_args!("supports extensions {}.\r\n", *e as char));
        }
    }
}

// Volume II: RISC-V Privileged Architectures V1.12-draft: Machine Timer Registers (mtime and mtimecmp)
// "Platforms provide a real-time counter, exposed as a memory-mapped machine-mode read-write register, mtime."
// "The mtime register has a 64-bit precision on all RV32 and RV64 systems"
// Since the register is memory-mapped, we cannot access it like the other csrs.
fn time() -> u64 {
    // CLINT = Core Local Interruptor
    // basically, a interrupt controller, which should allow us to get timer interrupts.
    // the qemu virt board seems to use the sifive clint.
    //
    // https://github.com/qemu/qemu/blob/7cc0cdcd6a771010ca4a4857c4e4df966bb4e6c2/hw/riscv/virt.c
    let virt_clint = 0x2000000;
    // https://github.com/qemu/qemu/blob/95a9457fd44ad97c518858a4e1586a5498f9773c/include/hw/riscv/sifive_clint.h
    let sifive_time_base = 0xBFF8;
    let time;
    unsafe {
        time = core::intrinsics::volatile_load((virt_clint + sifive_time_base) as *const u64);
    }
    time
}

fn time_sec() -> u64 {
    let sifive_clint_timebase_freq = 10000000;
    time() / sifive_clint_timebase_freq
}

fn set_timer_interrupt(time: u64) {
    let virt_clint = 0x2000000;
    let sifive_timecmp_base = 0x4000;
    unsafe {
        core::intrinsics::volatile_store((virt_clint + sifive_timecmp_base) as *mut u64, time);
    }
}

//static mut GLOBAL_INT: i32 = 42;

#[no_mangle] // our boot code literally calls my_main.
#[inline(never)] // called from _start, where no stack is ready. This is the first function with a real prologue and stack setup.
pub fn my_main(){
    uart::init();

    dump_isa();
    enable_interrupts();


    uart::println("Hello, World!");
    //unsafe {uart::printf(format_args!("GLOBAL_INT: {} at {:p}\r\n", GLOBAL_INT, &GLOBAL_INT));}

    // causes a machine trap
    //unsafe { asm!("ebreak"); }

    uart::println("Entering echo mode.");

    // causes Illegal Instruction
    //let time: u32;
    //unsafe{asm!("rdtime $0" : "=r"(time));}

    loop {
        let c = uart::getc();
        uart::printf(format_args!("-> {} \r\n", c as char));

        // cause a timer interrupt in 2 seconds (TRAPs and aborts, but shows that interrupts work)
        //set_timer_interrupt(time() + 10000000*2); // interrupt in 2 secs.
        uart::printf(format_args!("uptime: {} ({}s)\r\n", time(), time_sec()));

        dump_interrupt_status();

        if c == 'q' as u8 {
            break
        }
    }

    poweroff();

}
 
fn poweroff() {
    // https://groups.google.com/a/groups.riscv.org/forum/#!topic/sw-dev/IET9LBFJohU
    // https://github.com/qemu/qemu/blob/afd760539308a5524accf964107cdb1d54a059e3/hw/riscv/virt.c#L59
    // https://github.com/michaeljclark/riscv-probe/blob/master/libfemto/drivers/sifive_test.c
    let t: *mut u32 = 0x100000 as *mut u32;
    unsafe {
        core::intrinsics::volatile_store(t, 0x5555);
    }
}
