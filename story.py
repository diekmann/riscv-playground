#!/usr/bin/env python3

def inc(path):
    with open(path) as f:
        print()
        print(f"File {path}:")
        print("""```""")
        print(f.read())
        print("""```""")
        print()


print("""The machine.""")
inc('hello/.cargo/config')

print("""Linking""")
inc('hello/link.ld')


inc('hello/src/machine_setup.rs')
inc('hello/src/rust_setup.rs')
inc('hello/src/uart.rs')
inc('hello/src/main.rs')
