Bare metal hello world on RISC V in rust.

Target: qemu virt machine.

Requires nightly rust for bootstrapping assembly.

Cross-compiling with rust is nice and easy. No complicated tooling setup.

Does not require any additional riscv tools. {gcc, objcopy, ...} for riscv are not required. gdb-multiarch is helpful :-)
Only requires rust nightly (from upstream). The verisons of qemu and gdb-multiarch shipped in debian buster (currently: stable) are sufficient :-)
